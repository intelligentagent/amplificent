Amplificent
-----------

These are the Eagle schematics and layouts necessary to make 
the 45 W guitar amp from redcuircuits.com. All credits on the 
schematics, both for the amp and the PSU goes to them, this is just an 
implementation of their work. 

The boards have been produced and works, but there is one error. 
One of the input capacitors is switched around, so double check thatbefore assembly. 

The boards can be ordered from seeedstudio.com at $1 a peice. 
